
const { calculate } = require('./calculate');

module.exports.hello = (event, context, callback) => {
  const operation = (event.queryStringParameters || {}).Operation || '+';
  const a = parseInt((event.queryStringParameters || {}).a);
  const b = parseInt((event.queryStringParameters || {}).b);
  const result = calculate(operation, a, b);
  const SelectedOpt = [];
  SelectedOpt[operation] = 'selected';
  const html = `
  <html>
      <form action = "/dev/hello" method="GET">
      <input type="number"  name="a" value=${a}>
      <select name="Operation">
      <option value="+" ${SelectedOpt['+']}>+</option>
      <option value="-" ${SelectedOpt['-']}>-</option>
      <option value="/" ${SelectedOpt['/']}>/</option>
      <option value="*" ${SelectedOpt['*']}>*</option>
      <option value=">" ${SelectedOpt['>']}>></option>
      <option value="<" ${SelectedOpt['<']}><</option>
      <option value="=" ${SelectedOpt['=']}>=</option> 
      <option value=">=" ${SelectedOpt['>=']}>>=</option>
      <option value="<=" ${SelectedOpt['<=']}><=</option>
    </select>
    <input type="number" name="b" value=${b}>
    <input type="submit" value="=" >
    <strong id="result">${result}</strong>
    </form>
    </body>
  </html>`;

  const response = {
    statusCode: 200,
    headers: {
      'Content-Type': 'text/html',
    },
    body: html,
  };
  callback(null, response);
};
