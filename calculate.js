function calculate(Operation, a, b) {
  const math = {
    '+': function (x, y) { return x + y; },
    '-': function (x, y) { return x - y; },
    '/': function (x, y) { return x / y; },
    '*': function (x, y) { return x * y; },
    '>': function (x, y) { return x > y; },
    '<': function (x, y) { return x < y; },
    '>=': function (x, y) { return x >= y; },
    '<=': function (x, y) { return x <= y; },
    '=': function (x, y) { return x === y; },
  };
  // console.log('test');
  return math[Operation](a, b);
}

exports.calculate = calculate;
